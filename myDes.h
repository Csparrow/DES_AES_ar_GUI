#ifndef MYDES_H_INCLUDED
#define MYDES_H_INCLUDED


typedef unsigned long long ull;

class my_DES{
private:

    ull atslegas[16];

    struct bloka_dalas{
        ull L;
        ull R;
    };

    ///sakas metodes

    bool korekts_hex_s(const char s[]);

    void add_padding(char s[], const int bitu_sk_1bloka);

    //////////////////////////////////////////////////////////

    ull uztaisa_bit_variaciju(const ull vecais_skaitlis, const int vecais_bitu_sk, const int jaunais_bitu_sk, const int tabula[]);

    void uztaisa_atslegas(const ull sakotneja_atslega_64b);

    ull funkcija_f(const ull prev_R_puse_32b, const ull cur_rounda_atslega);

    ull nosifre_Bloku(const ull bloks);

//////////////////////////////////////////////////////////

public:

        bool set_key(const char key[]);

        bool en_decrypt_msg(const char init_vector[], const char msg[], char rez[], const bool encrypt);


};//my DES


#endif // MYDES_H_INCLUDED
