#ifndef _CRT_RAND_S
#define _CRT_RAND_S
#endif
#include <cstdlib>

/// pirmas 4 rindinas prieksh rand_c generatora

#if defined(UNICODE) && !defined(_UNICODE)
    #define _UNICODE
#elif defined(_UNICODE) && !defined(UNICODE)
    #define UNICODE
#endif

#include <tchar.h>
#include <windows.h>
#include <cstdio>
#include <string>

#include "myDes.h"
#include "myAES.h"

/*  Declare Windows procedure  */
LRESULT CALLBACK WindowProcedure (HWND, UINT, WPARAM, LPARAM);

/*  Make the class name into a global variable  */
TCHAR szClassName[ ] = _T("CodeBlocksWindowsApp");


HWND Btn_des, Btn_aes128, Btn_aes192, Btn_aes256, Btn_encrypt, Btn_decrypt, Btn_gen_IV64, Btn_gen_IV128;
HWND curalg_txt_field;
HWND key_txt_field;
HWND IV_txt_field;
HWND input_txt_field;
HWND output_txt_field;


std::string chosen_alg;
const int max_key_len=65;
const int max_txt_len=2049;
const int des_btn_id=1, aes128_btn_id=2, aes192_btn_id=3, aes256_btn_id=4;
const int encr_btn_id=5,decr_btn_id=6;
const int gen_IV64_btn_id=7, gen_IV128_btn_id=8;


char key[max_key_len];
char init_vector[130];//jo bloki: 64 vai 128
char input_txt[max_txt_len];
char output_txt[max_txt_len];

my_DES DES1;
my_AES AES1;
bool ok;
int aes_bitu_sk;


void gen_iv(const int bitu_sk){
    uint32_t cur;
    int skaits;
    char *p=init_vector;
    if(bitu_sk==64) skaits = 2; //2 integeri veido 64 bitus
    else skaits=4; //4 integeri veido 128 bitus

    for(int i=0; i<skaits; ++i){
        rand_s(&cur);
        sprintf(p, "%.8x", cur);
        p+=8;
    }

}//gen_iv




int WINAPI WinMain (HINSTANCE hThisInstance,
                     HINSTANCE hPrevInstance,
                     LPSTR lpszArgument,
                     int nCmdShow)
{
    HWND hwnd;               /* This is the handle for our window */
    MSG messages;            /* Here messages to the application are saved */
    WNDCLASSEX wincl;        /* Data structure for the windowclass */

    /* The Window structure */
    wincl.hInstance = hThisInstance;
    wincl.lpszClassName = szClassName;
    wincl.lpfnWndProc = WindowProcedure;      /* This function is called by windows */
    wincl.style = CS_DBLCLKS;                 /* Catch double-clicks */
    wincl.cbSize = sizeof (WNDCLASSEX);

    /* Use default icon and mouse-pointer */
    wincl.hIcon = LoadIcon (NULL, IDI_APPLICATION);
    wincl.hIconSm = LoadIcon (NULL, IDI_APPLICATION);
    wincl.hCursor = LoadCursor (NULL, IDC_ARROW);
    wincl.lpszMenuName = NULL;                 /* No menu */
    wincl.cbClsExtra = 0;                      /* No extra bytes after the window class */
    wincl.cbWndExtra = 0;                      /* structure or the window instance */
    /* Use Windows's default colour as the background of the window */
    wincl.hbrBackground = (HBRUSH) COLOR_BACKGROUND;

    /* Register the window class, and if it fails quit the program */
    if (!RegisterClassEx (&wincl))
        return 0;

    /* The class is registered, let's create the program*/
    hwnd = CreateWindowEx (
           0,                   /* Extended possibilites for variation */
           szClassName,         /* Classname */
           _T("AES & DES"),       /* Title Text */
           WS_OVERLAPPEDWINDOW, /* default window */
           CW_USEDEFAULT,       /* Windows decides the position */
           CW_USEDEFAULT,       /* where the window ends up on the screen */
           800,                 /* The programs width */
           540,                 /* and height in pixels */
           HWND_DESKTOP,        /* The window is a child-window to desktop */
           NULL,                /* No menu */
           hThisInstance,       /* Program Instance handler */
           NULL                 /* No Window Creation data */
           );

    /* Make the window visible on the screen */
    ShowWindow (hwnd, nCmdShow);

    /* Run the message loop. It will run until GetMessage() returns 0 */
    while (GetMessage (&messages, NULL, 0, 0))
    {
        /* Translate virtual-key messages into character messages */
        TranslateMessage(&messages);
        /* Send message to WindowProcedure */
        DispatchMessage(&messages);
    }

    /* The program return-value is 0 - The value that PostQuitMessage() gave */
    return messages.wParam;
}


/*  This function is called by the Windows function DispatchMessage()  */

LRESULT CALLBACK WindowProcedure (HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{

    switch (message)                  /* handle the messages */
    {
        case WM_CREATE:

            chosen_alg="DES";
            Btn_des = CreateWindow("BUTTON",
                         "DES-64",
                         WS_VISIBLE | WS_CHILD | WS_BORDER,
                         30, 20, 70, 20,
                         hwnd, (HMENU) des_btn_id, NULL, NULL);
            Btn_aes128 = CreateWindow("BUTTON",
                         "AES-128",
                         WS_VISIBLE | WS_CHILD | WS_BORDER,
                         140, 20, 70, 20,
                         hwnd, (HMENU) aes128_btn_id, NULL, NULL);
            Btn_aes192 = CreateWindow("BUTTON",
                         "AES-192",
                         WS_VISIBLE | WS_CHILD | WS_BORDER,
                         250, 20, 70, 20,
                         hwnd, (HMENU) aes192_btn_id, NULL, NULL);
            Btn_aes256 = CreateWindow("BUTTON",
                         "AES-256",
                         WS_VISIBLE | WS_CHILD | WS_BORDER,
                         360, 20, 70, 20,
                         hwnd, (HMENU) aes256_btn_id, NULL, NULL);



            CreateWindow("STATIC",
                         "CUR ALG:",
                         WS_VISIBLE | WS_CHILD | WS_DISABLED,
                         30, 60, 70, 15,
                         hwnd, NULL, NULL, NULL);

            curalg_txt_field = CreateWindow("EDIT",
                                     "DES-64",
                                     WS_VISIBLE | WS_CHILD | ES_READONLY,
                                     110, 60, 70, 15,
                                     hwnd, NULL, NULL, NULL);


            CreateWindow("STATIC",
                         "Key:",
                         WS_VISIBLE | WS_CHILD | WS_DISABLED,
                         30, 100, 30, 20,
                         hwnd, NULL, NULL, NULL);

            key_txt_field = CreateWindow("EDIT",
                                     "",
                                     WS_VISIBLE | WS_CHILD | ES_AUTOHSCROLL,
                                     90, 100, 520, 25,
                                     hwnd, NULL, NULL, NULL);


            CreateWindow("STATIC",
                         "IV:",
                         WS_VISIBLE | WS_CHILD | WS_DISABLED,
                         30, 140, 30, 20,
                         hwnd, NULL, NULL, NULL);

            IV_txt_field = CreateWindow("EDIT",
                                     "",
                                     WS_VISIBLE | WS_CHILD,
                                     90, 140, 260, 25,
                                     hwnd, NULL, NULL, NULL);

            Btn_gen_IV64 = CreateWindow("BUTTON",
                         "Gen IV-64",
                         WS_VISIBLE | WS_CHILD | WS_BORDER,
                         370, 140, 80, 25,
                         hwnd, (HMENU) gen_IV64_btn_id, NULL, NULL);

            Btn_gen_IV128 = CreateWindow("BUTTON",
                         "Gen IV-128",
                         WS_VISIBLE | WS_CHILD | WS_BORDER,
                         460, 140, 80, 25,
                         hwnd, (HMENU) gen_IV128_btn_id, NULL, NULL);



            CreateWindow("STATIC",
                         "Input:",
                         WS_VISIBLE | WS_CHILD | WS_DISABLED,
                         30, 180, 40, 20,
                         hwnd, NULL, NULL, NULL);

            input_txt_field = CreateWindow("EDIT",
                                     "",
                                     WS_VISIBLE | WS_CHILD | WS_BORDER | ES_MULTILINE,
                                     90, 180, 520, 100,
                                     hwnd, NULL, NULL, NULL);

            CreateWindow("STATIC",
                         "Output:",
                         WS_VISIBLE | WS_CHILD | WS_DISABLED,
                         30, 320, 50, 20,
                         hwnd, NULL, NULL, NULL);

            output_txt_field = CreateWindow("EDIT",
                                     "",
                                     WS_VISIBLE | WS_CHILD | WS_BORDER | ES_MULTILINE,
                                     90, 320, 520, 100,
                                     hwnd, NULL, NULL, NULL);


            Btn_encrypt = CreateWindow("BUTTON",
                             "Encrypt",
                             WS_VISIBLE | WS_CHILD | WS_BORDER,
                             200, 440, 80, 20,
                             hwnd, (HMENU) encr_btn_id, NULL, NULL);

            Btn_decrypt = CreateWindow("BUTTON",
                             "Decrypt",
                             WS_VISIBLE | WS_CHILD | WS_BORDER,
                             400, 440, 80, 20,
                             hwnd, (HMENU) decr_btn_id, NULL, NULL);

            break;

        case WM_COMMAND:
            switch(LOWORD(wParam))
            {
                case des_btn_id: // when button is clicked, this will happen:
                    if ((HWND)lParam == Btn_des) {
                         //GetWindowText(TextBox, buf, 10);
                         //SetWindowText(TextField,buf);
                         SetWindowText(curalg_txt_field,"DES-64");
                         chosen_alg="DES";
                    }
                    break;
                case aes128_btn_id: // when button is clicked, this will happen:
                    if ((HWND)lParam == Btn_aes128) {
                         SetWindowText(curalg_txt_field,"AES-128");
                         chosen_alg="AES-128";
                    }
                    break;
                case aes192_btn_id: // when button is clicked, this will happen:
                    if ((HWND)lParam == Btn_aes192) {
                         SetWindowText(curalg_txt_field,"AES-192");
                         chosen_alg="AES-192";
                    }
                    break;
                case aes256_btn_id: // when button is clicked, this will happen:
                    if ((HWND)lParam == Btn_aes256) {
                         SetWindowText(curalg_txt_field,"AES-256");
                         chosen_alg="AES-256";
                    }
                    break;

                case gen_IV64_btn_id: // when button is clicked, this will happen:
                    if ((HWND)lParam == Btn_gen_IV64) {
                         gen_iv(64);
                         SetWindowText(IV_txt_field, init_vector);
                    }
                    break;
                case gen_IV128_btn_id: // when button is clicked, this will happen:
                    if ((HWND)lParam == Btn_gen_IV128) {
                         gen_iv(128);
                         SetWindowText(IV_txt_field, init_vector);
                    }
                    break;
                case encr_btn_id: // when button is clicked, this will happen:
                    if ((HWND)lParam == Btn_encrypt) {
                         GetWindowText(key_txt_field, key, max_key_len);
                         GetWindowText(IV_txt_field, init_vector, 130);
                         GetWindowText(input_txt_field, input_txt, max_txt_len);
                         if(chosen_alg=="DES"){
                            if(DES1.set_key(key)) ok = DES1.en_decrypt_msg(init_vector, input_txt, output_txt, true);
                            else ok=false;
                         }
                         else{
                            if(chosen_alg=="AES-128") aes_bitu_sk=128;
                            else if(chosen_alg=="AES-192") aes_bitu_sk=192;
                            else aes_bitu_sk=256;
                            printf("key:%s\n",key);

                            if(AES1.set_Key(key, aes_bitu_sk)){
                                ok = AES1.en_decrypt_msg(init_vector, input_txt, output_txt, true);
                            }
                            else{
                                ok=false;
                            }
                         }
                         if(ok){
                            SetWindowText(output_txt_field, output_txt);
                         }
                         else{
                            ::MessageBox(hwnd, "Netika noradits pareizs key vai IV vai input teksts", "Error", MB_OK);
                         }

                    }
                    break;
                case decr_btn_id: // when button is clicked, this will happen:
                    if ((HWND)lParam == Btn_decrypt) {
                         GetWindowText(key_txt_field, key, max_key_len);
                         GetWindowText(IV_txt_field, init_vector, 130);
                         GetWindowText(input_txt_field, input_txt, max_txt_len);
                         if(chosen_alg=="DES"){
                            if(DES1.set_key(key)) ok = DES1.en_decrypt_msg(init_vector, input_txt, output_txt, false);
                            else ok=false;
                         }
                         else{
                            if(chosen_alg=="AES-128") aes_bitu_sk=128;
                            else if(chosen_alg=="AES-192") aes_bitu_sk=192;
                            else aes_bitu_sk=256;

                            if(AES1.set_Key(key, aes_bitu_sk)) ok = AES1.en_decrypt_msg(init_vector, input_txt, output_txt, false);
                            else ok=false;
                         }
                         if(ok){
                            SetWindowText(output_txt_field, output_txt);
                         }
                         else{
                            ::MessageBox(hwnd, "Netika noradits pareizs key vai IV vai input teksts", "Error", MB_OK);
                         }

                    }
                    break;
            }
            break;

        case WM_DESTROY:
            PostQuitMessage (0);       /* send a WM_QUIT to the message queue */
            break;
        default:                      /* for messages that we don't deal with */
            return DefWindowProc (hwnd, message, wParam, lParam);
    }

    return 0;
}
