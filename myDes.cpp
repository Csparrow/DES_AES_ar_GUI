#include <iostream>
#include <cstdio>
#include <cstring>

#include "myDes.h"

using namespace std;


typedef unsigned long long ull;




    bool my_DES::korekts_hex_s(const char s[]){
        const int len=strlen(s);

        if((len&1)==1){
            //ja nav para garums
            return false;
        }

        for(uint8_t cur, i=0; i<len; ++i){
            cur=s[i];
            if(!isdigit(cur) && (cur!='a' && cur!='b' && cur!='c'
               && cur!='d' && cur!='e' && cur!='f') &&
               (cur!='A' && cur!='B' && cur!='C'
               && cur!='D' && cur!='E' && cur!='F') ){
                   return false;
            }
        }
        return true;
    }//korekts_hex_s



    void my_DES::add_padding(char s[], const int bitu_sk_1bloka){
        int len=strlen(s);
        const int hex_simb_sk_1bloka=(bitu_sk_1bloka>>2);

        int bloku_sk = (len/hex_simb_sk_1bloka);
        if((len%hex_simb_sk_1bloka)!=0)bloku_sk++;

        int cik_japieliek = bloku_sk*hex_simb_sk_1bloka - len;

        for(int i=0,k=0; k<cik_japieliek; k+=2){
            s[i]='5';
            s[i+1]='c';
            i+=2;
        }
        s[cik_japieliek+len]=0;

    }//add_padding


    //////////////////////////////////////////////////////////

    ull my_DES::uztaisa_bit_variaciju(const ull vecais_skaitlis, const int vecais_bitu_sk, const int jaunais_bitu_sk, const int tabula[]){

        ull jaunais_skaitlis=0;
        int cur_bita_v;

        for(int i=0; i<jaunais_bitu_sk; ++i){
            jaunais_skaitlis = jaunais_skaitlis<<1;
            cur_bita_v = (vecais_skaitlis>>(vecais_bitu_sk-tabula[i]))&1;//noskaidro bita, kur indekss tabulaa, vertibu
            jaunais_skaitlis+=cur_bita_v; //jaunajam skaitlim uzstada
        }

        return jaunais_skaitlis;

    }//uztaisa_bit_variaciju


    void my_DES::uztaisa_atslegas(const ull sakotneja_atslega_64b){


        const int key_perm_table[56] = {
            57, 49, 41, 33, 25, 17, 9,
            1, 58, 50, 42, 34, 26, 18,
            10, 2, 59, 51, 43, 35, 27,
            19, 11, 3, 60, 52, 44, 36,

            63, 55, 47, 39, 31, 23, 15,
            7, 62, 54, 46, 38, 30, 22,
            14, 6, 61, 53, 45, 37, 29,
            21, 13, 5, 28, 20, 12, 4
        };



        const int key_compression_perm_table[48]={
            14, 17, 11, 24, 1, 5,
            3, 28, 15, 6, 21, 10,
            23, 19, 12, 4, 26, 8,
            16, 7, 27, 20, 13, 2,
            41, 52, 31, 37, 47, 55,
            30, 40, 51, 45, 33, 48,
            44, 49, 39, 56, 34, 53,
            46, 42, 50, 36, 29, 32
        };


        const int number_key_bits_shifted_per_round[16]={1, 1, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 1};

        ull starta_atslega_56b;
        ull cur_atslega_48b;
        int cur_ind;
        int kopa_shiftots=0;
        int cur_bita_v;


        starta_atslega_56b = uztaisa_bit_variaciju(sakotneja_atslega_64b, 64, 56, key_perm_table);


        for(int cur_round=0; cur_round<16; ++cur_round){

            cur_atslega_48b=0;
            kopa_shiftots += number_key_bits_shifted_per_round[cur_round];

            for(int cur_b=0; cur_b<48; ++cur_b){
                cur_atslega_48b = cur_atslega_48b<<1;

                cur_ind = key_compression_perm_table[cur_b];

                ///cur_ind = cur_ind - kopa_shiftots + 28;//si rindina it ka nosimule shiftosanu - reali neko neshifto
                if(cur_ind<29) cur_ind = (cur_ind-1+kopa_shiftots)%28 +1;
                else cur_ind = (cur_ind-29+kopa_shiftots)%28 +29;


                cur_bita_v = (starta_atslega_56b>>(56-cur_ind))&1;//noskaidro bita vertibu
                cur_atslega_48b+=cur_bita_v; //jaunajam skaitlim uzstada
            }


            atslegas[cur_round] = cur_atslega_48b;
        }


    }//uztaisa_atslegas



    ull my_DES::funkcija_f(const ull prev_R_puse_32b, const ull cur_rounda_atslega){

        ull rez=0;
        ull starp_rez48b;

        ull cur_ind;
        ull cur_box_rez;
        int cik_shiftot;

        const int expansion_perm_table[48]={
            32, 1, 2, 3, 4, 5,
            4, 5, 6, 7, 8, 9,
            8, 9, 10, 11, 12, 13,
            12, 13, 14, 15, 16, 17,
            16, 17, 18, 19, 20, 21,
            20, 21, 22, 23, 24, 25,
            24, 25, 26, 27, 28, 29,
            28, 29, 30, 31, 32, 1
        };


        const int S_boxes[8][64] = {
          {14, 0, 4, 15, 13, 7, 1, 4, 2, 14, 15, 2, 11, 13, 8, 1, 3, 10, 10, 6, 6, 12, 12, 11, 5, 9, 9, 5, 0, 3, 7, 8,
           4, 15, 1, 12, 14, 8, 8, 2, 13, 4, 6, 9, 2, 1, 11, 7, 15, 5, 12, 11, 9, 3, 7, 14, 3, 10, 10, 0, 5, 6, 0, 13},

          {15, 3, 1, 13, 8, 4, 14, 7, 6, 15, 11, 2, 3, 8, 4, 14, 9, 12, 7, 0, 2, 1, 13, 10, 12, 6, 0, 9, 5, 11, 10, 5,
           0, 13, 14, 8, 7, 10, 11, 1, 10, 3, 4, 15, 13, 4, 1, 2, 5, 11, 8, 6, 12, 7, 6, 12, 9, 0, 3, 5, 2, 14, 15, 9},

          {10, 13, 0, 7, 9, 0, 14, 9, 6, 3, 3, 4, 15, 6, 5, 10, 1, 2, 13, 8, 12, 5, 7, 14, 11, 12, 4, 11, 2, 15, 8, 1,
           13, 1, 6, 10, 4, 13, 9, 0, 8, 6, 15, 9, 3, 8, 0, 7, 11, 4, 1, 15, 2, 14, 12, 3, 5, 11, 10, 5, 14, 2, 7, 12},

          {7, 13, 13, 8, 14, 11, 3, 5, 0, 6, 6, 15, 9, 0, 10, 3, 1, 4, 2, 7, 8, 2, 5, 12, 11, 1, 12, 10, 4, 14, 15, 9,
           10, 3, 6, 15, 9, 0, 0, 6, 12, 10, 11, 1, 7, 13, 13, 8, 15, 9, 1, 4, 3, 5, 14, 11, 5, 12, 2, 7, 8, 2, 4, 14},

          {2, 14, 12, 11, 4, 2, 1, 12, 7, 4, 10, 7, 11, 13, 6, 1, 8, 5, 5, 0, 3, 15, 15, 10, 13, 3, 0, 9, 14, 8, 9, 6,
           4, 11, 2, 8, 1, 12, 11, 7, 10, 1, 13, 14, 7, 2, 8, 13, 15, 6, 9, 15, 12, 0, 5, 9, 6, 10, 3, 4, 0, 5, 14, 3},

          {12, 10, 1, 15, 10, 4, 15, 2, 9, 7, 2, 12, 6, 9, 8, 5, 0, 6, 13, 1, 3, 13, 4, 14, 14, 0, 7, 11, 5, 3, 11, 8,
           9, 4, 14, 3, 15, 2, 5, 12, 2, 9, 8, 5, 12, 15, 3, 10, 7, 11, 0, 14, 4, 1, 10, 7, 1, 6, 13, 0, 11, 8, 6, 13},

          {4, 13, 11, 0, 2, 11, 14, 7, 15, 4, 0, 9, 8, 1, 13, 10, 3, 14, 12, 3, 9, 5, 7, 12, 5, 2, 10, 15, 6, 8, 1, 6,
           1, 6, 4, 11, 11, 13, 13, 8, 12, 1, 3, 4, 7, 10, 14, 7, 10, 9, 15, 5, 6, 0, 8, 15, 0, 14, 5, 2, 9, 3, 2, 12},

          {13, 1, 2, 15, 8, 13, 4, 8, 6, 10, 15, 3, 11, 7, 1, 4, 10, 12, 9, 5, 3, 6, 14, 11, 5, 0, 0, 14, 12, 9, 7, 2,
           7, 2, 11, 1, 4, 14, 1, 7, 9, 4, 12, 10, 14, 8, 2, 13, 0, 15, 6, 12, 10, 9, 13, 0, 15, 3, 3, 5, 5, 6, 8, 11}
    };


        const int P_box_perm_table[32] = {
            16, 7, 20, 21, 29, 12, 28, 17,
            1, 15, 23, 26, 5, 18, 31, 10,
            2, 8, 24, 14, 32, 27, 3, 9,
            19, 13, 30, 6, 22, 11, 4, 25
        };


        starp_rez48b = uztaisa_bit_variaciju(prev_R_puse_32b, 32, 48, expansion_perm_table);

        starp_rez48b = starp_rez48b ^ cur_rounda_atslega;



        ///sakas S-Box substitucija

        for(int box_ind=0; box_ind<8; ++box_ind){

            rez <<= 4;

            cik_shiftot = 42-box_ind*6;//(7-box_ind)*6

            cur_ind = (starp_rez48b>>cik_shiftot) & 63; //63 jo maskai vajag 6x'1'


            cur_box_rez = S_boxes[box_ind][cur_ind];

            rez += cur_box_rez;

        }

        rez = uztaisa_bit_variaciju(rez, 32, 32, P_box_perm_table);

        return rez;

    }//funkcija_f





    ull my_DES::nosifre_Bloku(const ull bloks){

        ull nosifretais_bloks=0;
        ull starta_bloks;


        const int initial_perm_table[64]={
            58, 50, 42, 34, 26, 18, 10, 2,
            60, 52, 44, 36, 28, 20, 12, 4,
            62, 54, 46, 38, 30, 22, 14, 6,
            64, 56, 48, 40, 32, 24, 16, 8,
            57, 49, 41, 33, 25, 17, 9, 1,
            59, 51, 43, 35, 27, 19, 11, 3,
            61, 53, 45, 37, 29, 21, 13, 5,
            63, 55, 47, 39, 31, 23, 15, 7
        };

        const int final_perm_table[64]={
            40, 8, 48, 16, 56, 24, 64, 32,
            39, 7, 47, 15, 55, 23, 63, 31,
            38, 6, 46, 14, 54, 22, 62, 30,
            37, 5, 45, 13, 53, 21, 61, 29,
            36, 4, 44, 12, 52, 20, 60, 28,
            35, 3, 43, 11, 51, 19, 59, 27,
            34, 2, 42, 10, 50, 18, 58, 26,
            33, 1, 41, 9, 49, 17, 57, 25
        };



        bloka_dalas prev_rounda_bloks, cur_rounda_bloks;


        starta_bloks = uztaisa_bit_variaciju(bloks, 64, 64, initial_perm_table);



        prev_rounda_bloks.R = (((ull)(1)<<32)-1) & starta_bloks;
        starta_bloks >>=32;
        prev_rounda_bloks.L = (((ull)(1)<<32)-1) & starta_bloks;


        for(int cur_rounds=0; cur_rounds<16; ++cur_rounds){

            cur_rounda_bloks.L = prev_rounda_bloks.R;
            cur_rounda_bloks.R = prev_rounda_bloks.L ^ funkcija_f(prev_rounda_bloks.R, atslegas[cur_rounds]);


            prev_rounda_bloks = cur_rounda_bloks;
        }


        nosifretais_bloks = cur_rounda_bloks.R;
        nosifretais_bloks <<= 32;
        nosifretais_bloks += cur_rounda_bloks.L;



        nosifretais_bloks = uztaisa_bit_variaciju(nosifretais_bloks, 64, 64, final_perm_table);



        return nosifretais_bloks;

    }//nosifre_Bloku



//////////////////////////////////////////////////////////


    bool my_DES::set_key(const char key[]){
        ull sak_key;

        if(!korekts_hex_s(key) || strlen(key)!=16){
            return false;
        }

        sscanf(key,"%llx", &sak_key);

        uztaisa_atslegas(sak_key);

        return true;

    }//uzstada_atslegas


    bool my_DES::en_decrypt_msg(const char init_vector[], const char msg[], char rez[], const bool encrypt){

        ull cur_blocks;
        //ull cur_rez;

        ull prev_blocks,prev_blocks2;

        char msg_cpy[2049];
        char *p;
        char cur_ch_msg_bloks[17];
        char cur_ch_rez[17];

        int bloku_skaits;

        if(!korekts_hex_s(init_vector) || strlen(init_vector)!=16 || !korekts_hex_s(msg)){
            return false;
        }

        strcpy(msg_cpy, msg);
        add_padding(msg_cpy, 64);

        bloku_skaits = (strlen(msg_cpy))>>4; //jeb dala ar 16

        sscanf(init_vector, "%llx", &prev_blocks2);
        p = msg_cpy;

        rez[0]=0;



        if(encrypt){
            for(int i=0; i<bloku_skaits; ++i){
                sscanf(p,"%16c", cur_ch_msg_bloks);
                sscanf(cur_ch_msg_bloks, "%llx", &cur_blocks);
                cur_blocks = cur_blocks ^ prev_blocks2;

                cur_blocks = nosifre_Bloku(cur_blocks);
                prev_blocks2 = cur_blocks;

                sprintf(cur_ch_rez, "%.16llx", cur_blocks);
                strcat(rez, cur_ch_rez);
                p+=16;
            }
        }
        else{

            //jaatsifre
            //atslegas reverso
            for(int i=0; i<8;++i){
                swap(atslegas[i], atslegas[15-i]);
            }

            for(int i=0; i<bloku_skaits; ++i){
                sscanf(p,"%16c", cur_ch_msg_bloks);
                sscanf(cur_ch_msg_bloks, "%llx", &cur_blocks);

                prev_blocks = cur_blocks;

                cur_blocks = nosifre_Bloku(cur_blocks);
                cur_blocks = cur_blocks ^ prev_blocks2;

                prev_blocks2 = prev_blocks;

                sprintf(cur_ch_rez, "%.16llx", cur_blocks);
                strcat(rez, cur_ch_rez);
                p+=16;
            }
        }


        return true;
    }//en_decrypt_msg



