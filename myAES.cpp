#include <iostream>
#include <cstdio>
#include <cstring>

#include "myAES.h"


using namespace std;









    bool my_AES::korekts_hex_s(const char s[]){
        const int len=strlen(s);

        if((len&1)==1){
            //ja nav para garums
            return false;
        }

        for(uint8_t cur, i=0; i<len; ++i){
            cur=s[i];
            if(!isdigit(cur) && (cur!='a' && cur!='b' && cur!='c'
               && cur!='d' && cur!='e' && cur!='f') &&
               (cur!='A' && cur!='B' && cur!='C'
               && cur!='D' && cur!='E' && cur!='F') ){
                   return false;
            }
        }
        return true;
    }//korekts_hex_s



    void my_AES::add_padding(char s[], const int bitu_sk_1bloka){
        int len=strlen(s);
        const int hex_simb_sk_1bloka=(bitu_sk_1bloka>>2);

        int bloku_sk = (len/hex_simb_sk_1bloka);
        if((len%hex_simb_sk_1bloka)!=0)bloku_sk++;

        int cik_japieliek = bloku_sk*hex_simb_sk_1bloka - len;

        for(int i=0,k=0; k<cik_japieliek; k+=2){
            s[i]='5';
            s[i+1]='c';
            i+=2;
        }
        s[cik_japieliek+len]=0;

    }//add_padding


    ////////////////////////////////////////////////////////

    int8_t my_AES::Sbox_fcija(const uint8_t baita_vert){

        const uint8_t sbox[256] =
         {
            0x63, 0x7C, 0x77, 0x7B, 0xF2, 0x6B, 0x6F, 0xC5, 0x30, 0x01, 0x67, 0x2B, 0xFE, 0xD7, 0xAB, 0x76,
            0xCA, 0x82, 0xC9, 0x7D, 0xFA, 0x59, 0x47, 0xF0, 0xAD, 0xD4, 0xA2, 0xAF, 0x9C, 0xA4, 0x72, 0xC0,
            0xB7, 0xFD, 0x93, 0x26, 0x36, 0x3F, 0xF7, 0xCC, 0x34, 0xA5, 0xE5, 0xF1, 0x71, 0xD8, 0x31, 0x15,
            0x04, 0xC7, 0x23, 0xC3, 0x18, 0x96, 0x05, 0x9A, 0x07, 0x12, 0x80, 0xE2, 0xEB, 0x27, 0xB2, 0x75,
            0x09, 0x83, 0x2C, 0x1A, 0x1B, 0x6E, 0x5A, 0xA0, 0x52, 0x3B, 0xD6, 0xB3, 0x29, 0xE3, 0x2F, 0x84,
            0x53, 0xD1, 0x00, 0xED, 0x20, 0xFC, 0xB1, 0x5B, 0x6A, 0xCB, 0xBE, 0x39, 0x4A, 0x4C, 0x58, 0xCF,
            0xD0, 0xEF, 0xAA, 0xFB, 0x43, 0x4D, 0x33, 0x85, 0x45, 0xF9, 0x02, 0x7F, 0x50, 0x3C, 0x9F, 0xA8,
            0x51, 0xA3, 0x40, 0x8F, 0x92, 0x9D, 0x38, 0xF5, 0xBC, 0xB6, 0xDA, 0x21, 0x10, 0xFF, 0xF3, 0xD2,
            0xCD, 0x0C, 0x13, 0xEC, 0x5F, 0x97, 0x44, 0x17, 0xC4, 0xA7, 0x7E, 0x3D, 0x64, 0x5D, 0x19, 0x73,
            0x60, 0x81, 0x4F, 0xDC, 0x22, 0x2A, 0x90, 0x88, 0x46, 0xEE, 0xB8, 0x14, 0xDE, 0x5E, 0x0B, 0xDB,
            0xE0, 0x32, 0x3A, 0x0A, 0x49, 0x06, 0x24, 0x5C, 0xC2, 0xD3, 0xAC, 0x62, 0x91, 0x95, 0xE4, 0x79,
            0xE7, 0xC8, 0x37, 0x6D, 0x8D, 0xD5, 0x4E, 0xA9, 0x6C, 0x56, 0xF4, 0xEA, 0x65, 0x7A, 0xAE, 0x08,
            0xBA, 0x78, 0x25, 0x2E, 0x1C, 0xA6, 0xB4, 0xC6, 0xE8, 0xDD, 0x74, 0x1F, 0x4B, 0xBD, 0x8B, 0x8A,
            0x70, 0x3E, 0xB5, 0x66, 0x48, 0x03, 0xF6, 0x0E, 0x61, 0x35, 0x57, 0xB9, 0x86, 0xC1, 0x1D, 0x9E,
            0xE1, 0xF8, 0x98, 0x11, 0x69, 0xD9, 0x8E, 0x94, 0x9B, 0x1E, 0x87, 0xE9, 0xCE, 0x55, 0x28, 0xDF,
            0x8C, 0xA1, 0x89, 0x0D, 0xBF, 0xE6, 0x42, 0x68, 0x41, 0x99, 0x2D, 0x0F, 0xB0, 0x54, 0xBB, 0x16
         };

         return sbox[baita_vert];

    }//Sbox_fcija




    uint32_t my_AES::g(uint32_t el, int cur_rounds){

        uint32_t rez;
        uint8_t *p1, *p2;


        const uint8_t rcon[15] = {0x8d, 0x01, 0x02, 0x04,
        0x08, 0x10, 0x20, 0x40, 0x80, 0x1b, 0x36,
        0x6c, 0xd8, 0xab, 0x4d};


        p1 = (uint8_t *)&el;
        p2 = (uint8_t *)&rez;

        p2[3] = Sbox_fcija(p1[2]) ^ rcon[cur_rounds];
        p2[2] = Sbox_fcija(p1[1]);
        p2[1] = Sbox_fcija(p1[0]);
        p2[0] = Sbox_fcija(p1[3]);

        return rez;
    }//g



    uint32_t my_AES::h(uint32_t el){
        uint32_t rez;
        uint8_t *p1, *p2;


        p1 = (uint8_t *)&el;
        p2 = (uint8_t *)&rez;

        p2[0] = Sbox_fcija(p1[0]);
        p2[1] = Sbox_fcija(p1[1]);
        p2[2] = Sbox_fcija(p1[2]);
        p2[3] = Sbox_fcija(p1[3]);

        return rez;
    }//h




    void my_AES::add_round_key(uint8_t block[], const int round_nr){

        uint8_t *p = (uint8_t *)&word_Keys[round_nr<<2];
        int ind;

        for(int i=0; i<4; ++i){
            ind=i<<2;
            block[ind] = block[ind] ^ p[ind+3];
            block[ind+1] = block[ind+1] ^ p[ind+2];
            block[ind+2] = block[ind+2] ^ p[ind+1];
            block[ind+3] = block[ind+3] ^ p[ind];
        }

    }//add_round_key




    uint8_t my_AES::GaloisF_mult(uint8_t a, uint8_t b){
        uint8_t rez=0;

        while(a && b){

            if(b&1){
                //ja b pedejais bits ir 1
                rez ^= a; //pol-pieskaitam rezultatam a
            }
            if(a & 0x80){
                //ja ir koef pie x7, tad pareizinot ar x, vel jaizdala ar atl pol x^8 + x^4 + x^3 + x + 1, jeb vienkarsi xor
                a = (a<<1) ^ 0x11b;
            }
            else{
                //nebija koef pie x7, drosi var reizinat ar x
                a = a<<1;
            }
            b = b>>1; //izdala b ar x atmetot atlikumu
        }

        return rez;
    }//GaloisF_mult


    /////////////////////////////////////////////////////////

    void my_AES::byte_substitution(uint8_t block[]){
        for(int i=0; i<16; ++i){
            block[i] = Sbox_fcija(block[i]);;
        }
    }//byte_substitution



    void my_AES::shift_rows(uint8_t block[]){
        uint8_t tmp;


            ///0."rinda" nemainas

            ///1."rinda" <<1
            tmp=block[1];
            block[1]=block[5];
            block[5]=block[9];
            block[9]=block[13];
            block[13]=tmp;

            ///2."rinda" <<2
            swap(block[2],block[10]);
            swap(block[6],block[14]);


            ///3."rinda" <<3
            tmp=block[15];
            block[15]=block[11];
            block[11]=block[7];
            block[7]=block[3];
            block[3]=tmp;


    }//shift_rows



    void my_AES::mix_columns(uint8_t block[]){

        uint8_t tmp_block[18];
        uint32_t cur_rez;
        uint32_t tmp_rez;

        const int M[4][4]={
            {02,03,01,01},
            {01,02,03,01},
            {01,01,02,03},
            {03,01,01,02}
        };


        memcpy(&tmp_block, &block[0], 16);

        for(int k=0; k<16; k+=4){

            for(int i=0; i<4; ++i){
                cur_rez=0;
                for(int j=0; j<4; ++j){
                    if(M[i][j]==2){
                        tmp_rez = (tmp_block[k+j])<<1;
                    }
                    else if(M[i][j]==3){
                        tmp_rez = (tmp_block[k+j])<<1;
                        tmp_rez ^= tmp_block[k+j];
                    }
                    else{
                        tmp_rez = tmp_block[k+j];
                    }
                    if(((tmp_rez>>8)&1) !=0){
                        tmp_rez ^= 0x11b;
                    }

                    cur_rez ^= tmp_rez;
                }
                block[k+i]=cur_rez;
            }
        }

    }//mix_columns



    /////////////////////////////////////////////////////////

    int8_t my_AES::Inv_Sbox_fcija(const uint8_t baita_vert){

        const uint8_t inv_sbox[256] =
         {
            0x52, 0x09, 0x6A, 0xD5, 0x30, 0x36, 0xA5, 0x38, 0xBF, 0x40, 0xA3, 0x9E, 0x81, 0xF3, 0xD7, 0xFB,
            0x7C, 0xE3, 0x39, 0x82, 0x9B, 0x2F, 0xFF, 0x87, 0x34, 0x8E, 0x43, 0x44, 0xC4, 0xDE, 0xE9, 0xCB,
            0x54, 0x7B, 0x94, 0x32, 0xA6, 0xC2, 0x23, 0x3D, 0xEE, 0x4C, 0x95, 0x0B, 0x42, 0xFA, 0xC3, 0x4E,
            0x08, 0x2E, 0xA1, 0x66, 0x28, 0xD9, 0x24, 0xB2, 0x76, 0x5B, 0xA2, 0x49, 0x6D, 0x8B, 0xD1, 0x25,
            0x72, 0xF8, 0xF6, 0x64, 0x86, 0x68, 0x98, 0x16, 0xD4, 0xA4, 0x5C, 0xCC, 0x5D, 0x65, 0xB6, 0x92,
            0x6C, 0x70, 0x48, 0x50, 0xFD, 0xED, 0xB9, 0xDA, 0x5E, 0x15, 0x46, 0x57, 0xA7, 0x8D, 0x9D, 0x84,
            0x90, 0xD8, 0xAB, 0x00, 0x8C, 0xBC, 0xD3, 0x0A, 0xF7, 0xE4, 0x58, 0x05, 0xB8, 0xB3, 0x45, 0x06,
            0xD0, 0x2C, 0x1E, 0x8F, 0xCA, 0x3F, 0x0F, 0x02, 0xC1, 0xAF, 0xBD, 0x03, 0x01, 0x13, 0x8A, 0x6B,
            0x3A, 0x91, 0x11, 0x41, 0x4F, 0x67, 0xDC, 0xEA, 0x97, 0xF2, 0xCF, 0xCE, 0xF0, 0xB4, 0xE6, 0x73,
            0x96, 0xAC, 0x74, 0x22, 0xE7, 0xAD, 0x35, 0x85, 0xE2, 0xF9, 0x37, 0xE8, 0x1C, 0x75, 0xDF, 0x6E,
            0x47, 0xF1, 0x1A, 0x71, 0x1D, 0x29, 0xC5, 0x89, 0x6F, 0xB7, 0x62, 0x0E, 0xAA, 0x18, 0xBE, 0x1B,
            0xFC, 0x56, 0x3E, 0x4B, 0xC6, 0xD2, 0x79, 0x20, 0x9A, 0xDB, 0xC0, 0xFE, 0x78, 0xCD, 0x5A, 0xF4,
            0x1F, 0xDD, 0xA8, 0x33, 0x88, 0x07, 0xC7, 0x31, 0xB1, 0x12, 0x10, 0x59, 0x27, 0x80, 0xEC, 0x5F,
            0x60, 0x51, 0x7F, 0xA9, 0x19, 0xB5, 0x4A, 0x0D, 0x2D, 0xE5, 0x7A, 0x9F, 0x93, 0xC9, 0x9C, 0xEF,
            0xA0, 0xE0, 0x3B, 0x4D, 0xAE, 0x2A, 0xF5, 0xB0, 0xC8, 0xEB, 0xBB, 0x3C, 0x83, 0x53, 0x99, 0x61,
            0x17, 0x2B, 0x04, 0x7E, 0xBA, 0x77, 0xD6, 0x26, 0xE1, 0x69, 0x14, 0x63, 0x55, 0x21, 0x0C, 0x7D
         };

         return inv_sbox[baita_vert];

    }//Inv_Sbox_fcija



    void my_AES::inv_byte_substitution(uint8_t block[]){
        for(int i=0; i<16; ++i){
            block[i] = Inv_Sbox_fcija(block[i]);;
        }
    }//inv_byte_substitution



    void my_AES::inv_shift_rows(uint8_t block[]){
        uint8_t tmp;

            ///0."rinda" nemainas

            ///1."rinda" >>1
            tmp=block[13];
            block[13]=block[9];
            block[9]=block[5];
            block[5]=block[1];
            block[1]=tmp;


            ///2."rinda" >>2
            swap(block[2],block[10]);
            swap(block[6],block[14]);


            ///3."rinda" >>3
            tmp=block[3];
            block[3]=block[7];
            block[7]=block[11];
            block[11]=block[15];
            block[15]=tmp;


    }//inv_shift_rows




    void my_AES::inv_mix_columns(uint8_t block[]){

        uint8_t tmp_block[18];
        uint32_t cur_rez;
        uint32_t tmp_rez;

        const int M[4][4]={
            {0x0e, 0x0b, 0x0D, 0x09},
            {0x09, 0xe, 0x0b, 0x0d},
            {0x0d, 0x09, 0x0e, 0x0b},
            {0x0b, 0x0d, 0x09, 0x0e}
        };


        memcpy(&tmp_block, &block[0], 16);

        for(int k=0; k<16; k+=4){

            for(int i=0; i<4; ++i){
                cur_rez=0;
                for(int j=0; j<4; ++j){
                    tmp_rez = GaloisF_mult(M[i][j], tmp_block[k+j]);
                    cur_rez ^= tmp_rez;
                }
                block[k+i]=cur_rez;
            }
        }

    }//invmix_columns



    /////////////////////////////////////////////////////////

    void my_AES::sak_key_ielase(const char orig_key[], const int len){
        char temp_arr[65];
        char *p;

        strcpy(temp_arr,orig_key);

        p = &temp_arr[len-8];

        for(int i=Nk-1; i>=0; --i){
            sscanf(p, "%x", &word_Keys[i]);

            (*p)=0;
            p-=8;
        }
    }//sak_key_ielase



    void my_AES::sak_block_ielase(const char cur_hex_char_block[], const int len, uint8_t cur_block[]){
        char tmp[33];
        char *p;

        uint32_t t1;

        strcpy(tmp,cur_hex_char_block);

        p = &tmp[len-2];
        for(int i=15; i>=0; --i){
            sscanf(p, "%x", &t1);
            cur_block[i]=t1;
            (*p)=0;
            p-=2;
        }

    }//sak_block_ielase



    /////////////////////////////////////////////////////////

    void my_AES::encrypt_block(uint8_t cur_block[]){


        add_round_key(cur_block, 0);


        for(int cur_rounds=1; cur_rounds<Nr; ++cur_rounds){

            byte_substitution(cur_block);

            shift_rows(cur_block);

            mix_columns(cur_block);

            add_round_key(cur_block, cur_rounds);
        }

        byte_substitution(cur_block);
        shift_rows(cur_block);
        add_round_key(cur_block, Nr);


    }//encrypt_block



    void my_AES::decrypt_block(uint8_t cur_block[]){

        add_round_key(cur_block, Nr);


        for(int cur_rounds=Nr-1; cur_rounds>0; --cur_rounds){

            inv_shift_rows(cur_block);

            inv_byte_substitution(cur_block);

            add_round_key(cur_block, cur_rounds);

            inv_mix_columns(cur_block);

        }

        inv_shift_rows(cur_block);
        inv_byte_substitution(cur_block);
        add_round_key(cur_block, 0);



    }//decrypt_block



    /////////////////////////////////////////////////////////


    bool my_AES::set_Key(const char orig_key[], const int bitu_sk){

        int orig_key_len = strlen(orig_key);


        if(!korekts_hex_s(orig_key)){
            //cerr<<"Nekorekta atlsega"<<endl;
            return false;
        }


        if(bitu_sk == 128 && orig_key_len==32){
            //sanemta pareiza 128 bitu atslega
            Nr=10;
            Nk=4;
        }
        else if(bitu_sk==192 && orig_key_len==48){
            //sanemta 192 bitu atslega
            Nr=12;
            Nk=6;
        }
        else if(bitu_sk==256 && orig_key_len==64){
            //sanemta 256 bitu atslega
            Nr=14;
            Nk=8;
        }
        else{
            //cerr<<"Nekorekts inputs!!!"<<endl;
            return false;
        }

        cnt_word_Keys = 4*(Nr+1);
        sak_key_ielase(orig_key, orig_key_len);



        if(bitu_sk!=256){
            for(int rounds=1; rounds<=(4*Nr)/Nk; ++rounds){
                word_Keys[Nk*rounds] = word_Keys[Nk*(rounds-1)] ^ g(word_Keys[Nk*rounds -1],rounds);
                for(int j=1; j<Nk; ++j){
                    word_Keys[Nk*rounds+j] = word_Keys[Nk*rounds +j -1] ^ word_Keys[Nk*(rounds-1)+j];
                }
            }
        }
        else{
            for(int rounds=1; rounds<=7; ++rounds){
                word_Keys[Nk*rounds] = word_Keys[Nk*(rounds-1)] ^ g(word_Keys[Nk*rounds -1],rounds);

                word_Keys[Nk*rounds+1] = word_Keys[Nk*rounds] ^ word_Keys[Nk*(rounds-1)+1];
                word_Keys[Nk*rounds+2] = word_Keys[Nk*rounds+1] ^ word_Keys[Nk*(rounds-1)+2];
                word_Keys[Nk*rounds+3] = word_Keys[Nk*rounds+2] ^ word_Keys[Nk*(rounds-1)+3];

                word_Keys[Nk*rounds+4] = h(word_Keys[Nk*rounds+3]) ^ word_Keys[Nk*(rounds-1)+4];

                word_Keys[Nk*rounds+5] = word_Keys[Nk*rounds+4] ^ word_Keys[Nk*(rounds-1)+5];
                word_Keys[Nk*rounds+6] = word_Keys[Nk*rounds+5] ^ word_Keys[Nk*(rounds-1)+6];
                word_Keys[Nk*rounds+7] = word_Keys[Nk*rounds+6] ^ word_Keys[Nk*(rounds-1)+7];
            }
        }



        return true;

    }//set_key




    bool my_AES::en_decrypt_msg(const char init_vector[], const char orig_hex_msg[], char rez[], const bool encrypt){

        char hex_msg[2049];
        char cur_msg_bloks[33];
        char cur_rez_bloks[33];

        char *p;

        int len;
        int bloku_skaits;

        uint8_t cur_block[17];
        uint8_t prev_block[17];
        uint8_t prev_block2[17];


        rez[0]=0;//svarigi

        strcpy(hex_msg,orig_hex_msg);

        if(!korekts_hex_s(orig_hex_msg) || !korekts_hex_s(init_vector) || strlen(init_vector)!=32){
            return false;
        }
        add_padding(hex_msg, 128);

        len=strlen(hex_msg);
        bloku_skaits=(len>>5);//jo 1 bloka 2^5=32 hex simboli
        p = hex_msg;

        sak_block_ielase(init_vector, 32, prev_block);

        if(encrypt){
            for(int bloka_nr=0; bloka_nr<bloku_skaits; ++bloka_nr){
                sscanf(p,"%32c",cur_msg_bloks);
                sak_block_ielase(cur_msg_bloks, 32, cur_block);

                for(int i=0; i<16; ++i) cur_block[i] = cur_block[i] ^ prev_block[i];

                encrypt_block(cur_block);

                memcpy(prev_block, cur_block, 16);

                char *cur_rez_p=cur_rez_bloks;
                for(int i=0; i<16; ++i){
                    sprintf(cur_rez_p,"%.2x",cur_block[i]);
                    cur_rez_p+=2;
                }



                strcat(rez, cur_rez_bloks);
                p+=32;
            }//for beigas
        }
        else{
            for(int bloka_nr=0; bloka_nr<bloku_skaits; ++bloka_nr){
                sscanf(p,"%32c",cur_msg_bloks);
                sak_block_ielase(cur_msg_bloks, 32, cur_block);
                memcpy(prev_block2, cur_block, 16);

                decrypt_block(cur_block);

                for(int i=0; i<16; ++i) cur_block[i] = cur_block[i] ^ prev_block[i];


                memcpy(prev_block, prev_block2, 16);

                char *cur_rez_p=cur_rez_bloks;
                for(int i=0; i<16; ++i){
                    sprintf(cur_rez_p,"%.2x",cur_block[i]);
                    cur_rez_p+=2;
                }


                strcat(rez, cur_rez_bloks);
                p+=32;
            }//for beigas
        }



        return true;
    }//en_decrypt_msg



