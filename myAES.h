#ifndef MYAES_H_INCLUDED
#define MYAES_H_INCLUDED




class my_AES{
private:

    int Nr; //roundu skaits
    int Nk; //tik 32-bit words prieksh atslegu gen 1 roundaa
    uint32_t word_Keys[70];
    int cnt_word_Keys;



    bool korekts_hex_s(const char s[]);

    void add_padding(char s[], const int bitu_sk_1bloka);

    ////////////////////////////////////////////////////////

    int8_t Sbox_fcija(const uint8_t baita_vert);

    uint32_t g(uint32_t el, int cur_rounds);

    uint32_t h(uint32_t el);

    void add_round_key(uint8_t block[], const int round_nr);

    uint8_t GaloisF_mult(uint8_t a, uint8_t b);

    /////////////////////////////////////////////////////////

    void byte_substitution(uint8_t block[]);

    void shift_rows(uint8_t block[]);

    void mix_columns(uint8_t block[]);

    /////////////////////////////////////////////////////////

    int8_t Inv_Sbox_fcija(const uint8_t baita_vert);

    void inv_byte_substitution(uint8_t block[]);

    void inv_shift_rows(uint8_t block[]);

    void inv_mix_columns(uint8_t block[]);


    /////////////////////////////////////////////////////////

    void sak_key_ielase(const char orig_key[], const int len);

    void sak_block_ielase(const char cur_hex_char_block[], const int len, uint8_t cur_block[]);

    /////////////////////////////////////////////////////////

    void encrypt_block(uint8_t cur_block[]);

    void decrypt_block(uint8_t cur_block[]);

    /////////////////////////////////////////////////////////


public:

    bool set_Key(const char orig_key[], const int bitu_sk);

    bool en_decrypt_msg(const char init_vector[], const char orig_hex_msg[], char rez[], const bool encrypt);


};//end of my_AES



#endif // MYAES_H_INCLUDED
